// 
//

#include "stdafx.h"
#include <iostream>

int countunsetbits(int);

int main()
{
	int n = 12547;
	//std::cout << countunsetbits(n) << std::endl;
    return 0;
}
int countunsetbits(int n)
{
	int count = 0;

	// x holds one set digit at a time
	// starting from LSB to MSB of n.
	for (int x = 1; x <= n; x = x << 1)
		if ((x & n) == 0)
			count++;

	return count;
}

